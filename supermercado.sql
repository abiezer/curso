/*
crear una base de datos llamada supermercado
con una tabla producto
	id
	nombre
	codigo varchar
	precio
	stock
tabla llamada venta
	id
	fecha
	cantidad
	product_id
**calcular el monto que he vendido por concepto de cada producto
**se debe restar el stock

*/



CREATE TABLE Producto(
	id int identity Primary Key,
	nombre varchar(255),
	codigo varchar(255),
	precio float,
	stock int
)

CREATE TABLE Venta(
	id int identity Primary key,
	fecha date,
	cantidad int,
	product_id int foreign key references Producto(id)
)

insert into Producto Values('manzana','H7FB',2235,45);
insert into Producto Values('pera','H8FB',2235,45);

/*
* Una venta
*/
update Producto 
set stock=(select stock from Producto Where id=2)-4
where id=2;
insert into Venta Values('2018-12-6',4,2);

/*
* Otra venta
*/
update Producto 
set stock=(select stock from Producto Where id=2)-5
where id=2;
insert into Venta Values('2018-12-6',5,2);

/*
* Calcular monto total vendido de un producto
*/
Select precio * (select sum(cantidad) from venta where product_id=2)
as vendido
from Producto where id=2;

select * from producto
select * from venta