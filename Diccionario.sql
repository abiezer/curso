/*
*Creacion de tabla
*/
CREATE TABLE Palabras(
	id int identity PRIMARY KEY,
	palabra varchar(90),
	significado varchar(250)
)

CREATE TABLE Words(
	id int identity,
	words varchar(90), 
	meaning varchar(250),
	Palabras_id int NULL,
	FOREIGN KEY (Palabras_id) REFERENCES Palabras(id)
)
/*
*Borrar Tablas
*/
DROP TABLE Words;
DROP TABLE Palabras;

/*
*Modificar Tablas
*/
ALTER TABLE Palabras ADD otroCampo varchar(255);
ALTER TABLE Palabras ALTER COLUMN otrocampo varchar(200) NOT NULL;
ALTER TABLE Palabras DROP COLUMN otrocampo;


ALTER TABLE Palabras ADD PRIMARY KEY(id);
ALTER TABLE words ADD Palabras_id int NULL;
ALTER TABLE words ADD FOREIGN KEY (Palabras_id) REFERENCES Palabras(id);

/*
*Seleccion de Datos
*/
SELECT * FROM Palabras;
SELECT * FROM Palabras WHERE palabra like '%L%';
SELECT palabra, significado from Palabras;

/*
* Insercion de Datos
*/

INSERT INTO Palabras VALUES(
'mesa',
'es un objeto que mer permite poner cosas sobre el'
)

INSERT INTO Palabras(palabra, significado) VALUES(
'silla',
'es un objeto que me permite sentarme'
)

INSERT INTO Palabras(palabra, significado) VALUES(
'SILLAS',
'es el pural de sillas'
)

INSERT INTO Words VALUES(
'chair',
'This is a suport',
2
)
INSERT INTO Words VALUES(
'sofa',
'This is a suport',
2
)


/*
* Actualizacion de datos
*/
UPDATE Palabras 
SET palabra='SILLA', 
significado='nos permite sentarnos'
WHERE id=2;

UPDATE Words 
SET Palabras_id = 2;

/*
*Borrar Datos
*/
DELETE Words WHERE id=1 OR id=4


/*
* Uniones
*/

SELECT *
FROM  Words LEFT JOIN Palabras 
ON Palabras.id = Words.Palabras_id;