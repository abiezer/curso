/****** Script para el comando SelectTopNRows de SSMS  ******/
SELECT TOP (1000) [nombre]
      ,[apellido]
      ,[edad]
  FROM [curso].[dbo].[Estudiante];

/*
*ESTA ES UNA CONSULTA DE INSERCION
*/

INSERT INTO Estudiante(nombre, apellido, edad) VALUES('Jorge','Cifuentes',20);
INSERT INTO Estudiante(nombre, apellido, edad) VALUES('Pedro','Hernandez',17);
INSERT INTO Estudiante(nombre, apellido, edad) VALUES('Luis','Cifuentes',14);
INSERT INTO Estudiante(nombre, apellido, edad) VALUES('Petra','Perez',29);
INSERT INTO Estudiante(nombre, apellido, edad) VALUES('Ramon','Toledo',25);

/*
*ESTA ES UNA CONSULTA DE SELECCION
*/

SELECT * FROM Estudiante;
SELECT * FROM Profesor;
SELECT * FROM Ramo;


/*
* actualizar
*/
UPDATE dbo.Estudiante SET nombre='123',apellido='dsd' WHERE nombre='Jorge';

/*
*Eliminar
*/
DELETE dbo.Profesor WHERE nombre='Pablo';


CREATE TABLE Ramo(id int identity, nombre varchar(255),horas int, profesor_id int, estudiante_id int);
/*CREATE TABLE Asistenca(id int identity, fecha DATE,horas int, profesor_id int, estudiante_id int);*/

/*
*Borrar la Tabla ramo
*/
DROP TABLE curso.dbo.Ramo;

INSERT INTO Profesor(nombre) VALUES('kjlk') ;
INSERT INTO Profesor(nombre) VALUES('Pedro') ;
INSERT INTO Profesor(nombre) VALUES('Pablo') ;

INSERT INTO Ramo(nombre,horas,profesor_id,estudiante_id) VALUES ('Base de Datos',80,1,1);
INSERT INTO Asistenca(fecha,horas,profesor_id,estudiante_id) VALUES ('2018-01-01',80,1,1);
INSERT INTO Ramo(nombre,horas,profesor_id,estudiante_id)
VALUES ('Base de Datos',80,1,2);



SELECT * FROM Estudiante


SELECT Ramo.nombre as NombreRamo, Profesor.nombre as NombreProfesor
FROM Ramo inner join Profesor ON
Ramo.profesor_id = Profesor.id;






/*
*Modificar una Tabla
*/
ALTER TABLE dbo.Estudiante ADD id INT NOT NULL IDENTITY;
ALTER TABLE dbo.Asistencia DROP COLUMN id;


Create TABLE curso.dbo.Asistencia(id int identity,profesor_id int,estudiante_id int,ramo_id int,fecha date, horas int);

/*
*Vaciar Valores de la tabla
'2018-12-31'
*/
TRUNCATE TABLE Estudiante;


/*
*Horas Estudinates
*/
declare @suma int
set @suma = (SELECT SUM(Asistencia.horas) 
from Asistencia inner join Estudiante 
ON Asistencia.estudiante_id = Estudiante.id WHERE Estudiante.id=1);
SELECT (horas - @suma) AS HorasFaltantes, Ramo.nombre
FROM Ramo
WHERE Ramo.id=1;

/*
*SUM(horas) --suma de todos datos de la consulta
*MAX(horas) --La Hora mas alta
*MIN(horas) --La Hora mas baja
*/

SELECT
sum(Asistencia.horas)
as sumaTodos
FROM Asistencia iNNER JOIN Estudiante 
ON Asistencia.estudiante_id = Estudiante.id
WHERE estudiante_id=2;



INSERT INTO 
Asistencia(profesor_id,estudiante_id,ramo_id,fecha,horas)
VALUES(1,2,1,'2018-01-13',3);

Select * FROM Asistencia;
